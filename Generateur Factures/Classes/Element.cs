﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generateur_Factures.Classes
{
    public class Element
    {
      public  String Reference { get; set; }
      public  String Description { get; set; }
      public  float Prix_Ht_Actuel { get; set; }
      public  String Type { get; set; }

    }
}
