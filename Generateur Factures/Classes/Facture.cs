﻿using Generateur_Factures.Dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generateur_Factures.Classes
{
    public class Facture
    {
        public int Id { get; set; }
        public DateTime Date_Fact { get; set; }
        public int Num_Or { get; set; }
        public int Num_Act { get; set; }
        public String Accueil { get; set; }
        public DateTime Date_Paie { get; set; }
        public Dictionary<Element, Facture> ElementsFactures { get; set; }



        public static List<Facture> GetAll()
        {

            return FactureDao.GetAll();
        }
    }
}
