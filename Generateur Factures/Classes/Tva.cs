﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generateur_Factures.Classes
{
   public class Tva
    {
        public int Id { get; set; }
        public float Taux { get; set; }
    }
}
