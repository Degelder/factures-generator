﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generateur_Factures.Classes
{
public class Vehicule
    {
        public string Id  {get; set;}
        public string Immat  {get; set;}
        public string NumSerie  {get; set;}
        public DateTime Mec  {get; set;}
        public DateTime Livraison  {get; set;}
        public string Kms  {get; set;}
        public string Gamme  {get; set;}
        public string Modele  {get; set;}
        public string Tvv  {get; set;}
    }
}
