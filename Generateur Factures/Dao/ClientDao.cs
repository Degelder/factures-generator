﻿using Generateur_Factures.Classes;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generateur_Factures.Dao
{
    public class ClientDao
    {
        public static List<Client> GetAll()
        {

            List<Client> items = new List<Client>();
            MySqlConnection connection = Dao.Connection;
            try
            {
                connection.Close();
            }
            catch
            {

            }
            connection.Open();
            MySqlCommand cmd = connection.CreateCommand();
            cmd.CommandText = "SELECT * FROM `client`";
            cmd.Prepare();
            cmd.ExecuteNonQuery();
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Console.WriteLine("test");
                Client item = new Client
                {
                    Id = (int)reader["cl_id"],
                    Nom = (string)reader["cl_nom"],
                    Prenom = (string)reader["cl_prenom"],
                    Adresse = (string)reader["cl_adresse"],
                    Cp = (string)reader["cl_code_postale"],
                    Ville = (string)reader["cl_ville"],
                    Tel = (string)reader["cl_tel"],
                    Mobile = (string)reader["cl_mobile"],
                    NumCompte = (string)reader["cl_num_compte"]

                };
                items.Add(item);
            }
            connection.Close();

            return items;
        }
    }
}
