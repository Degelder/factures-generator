﻿using System;
using System.Configuration;
using System.Threading.Tasks;
using Microsoft.Owin;
using MySql.Data.MySqlClient;
using Owin;

[assembly: OwinStartup(typeof(Generateur_Factures.Dao.Dao))]

namespace Generateur_Factures.Dao
{
    public class Dao
    {
        private static MySqlConnection connection;

        public static MySqlConnection Connection
        {
            get
            {
                if (connection == null)
                {
                    MySqlConnectionStringBuilder str = new MySqlConnectionStringBuilder();
                    str.Server = ConfigurationManager.AppSettings.Get("Server");
                    str.Password = ConfigurationManager.AppSettings.Get("Password");
                    str.Port = Convert.ToUInt32(ConfigurationManager.AppSettings.Get("Port"));
                    str.UserID = ConfigurationManager.AppSettings.Get("User");
                    str.Database = ConfigurationManager.AppSettings.Get("Database");
                    str.CharacterSet = ConfigurationManager.AppSettings.Get("Charset"); ;
                    connection = new MySqlConnection(str.ToString());
                }
                return connection;
            }

        }
    }
}
