﻿using Generateur_Factures.Classes;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generateur_Factures.Dao
{
    public class ElementDao
    {
        public static List<Element> GetAll()
        {

            List<Element> items = new List<Element>();
            MySqlConnection connection = Dao.Connection;
            try
            {
                connection.Close();
            }
            catch
            {

            }
            connection.Open();
            MySqlCommand cmd = connection.CreateCommand();
            cmd.CommandText = "SELECT * FROM `element`";
            cmd.Prepare();
            cmd.ExecuteNonQuery();
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Console.WriteLine("test");
                Element item = new Element
                {
                    Reference = (string)reader["el_reference"],
                    Description = (string)reader["el_description"],
                    Prix_Ht_Actuel = (float)reader["el_prix_ht"],
                    Type = (string)reader["el_type"]
                };
                items.Add(item);
            }
            connection.Close();

            return items;
        }
    }
}
