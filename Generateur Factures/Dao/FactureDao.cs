﻿using Generateur_Factures.Classes;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generateur_Factures.Dao
{
    public class FactureDao
    {
        public static List<Facture> GetAll()
        {

            List<Facture> items = new List<Facture>();
            MySqlConnection connection = Dao.Connection;
            try
            {
                connection.Close();
            }
            catch
            {

            }
            connection.Open();
            MySqlCommand cmd = connection.CreateCommand();
            cmd.CommandText = "SELECT * FROM `facture`";
            cmd.Prepare();
            cmd.ExecuteNonQuery();
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Console.WriteLine("test");
                Facture item = new Facture
                {
                    Id = (int)reader["fa_id"],
                    Date_Fact = (DateTime)reader["fa_date_facture"],
                    Num_Or = (int)reader["fa_num_or"],
                    Num_Act = (int)reader["fa_num_action"],
                    Accueil = (string)reader["fa_accueil"],
                    Date_Paie = (DateTime)reader["fa_date_paiement"]
                };
                items.Add(item);
            }
            connection.Close();

            return items;
        }
    }
}
