﻿using Generateur_Factures.Classes;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generateur_Factures.Dao
{
    public class TvaDao
    {
        public static List<Tva> GetAll()
        {

            List<Tva> items = new List<Tva>();
            MySqlConnection connection = Dao.Connection;
            try
            {
                connection.Close();
            }
            catch
            {

            }
            connection.Open();
            MySqlCommand cmd = connection.CreateCommand();
            cmd.CommandText = "SELECT * FROM `tva`";
            cmd.Prepare();
            cmd.ExecuteNonQuery();
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Console.WriteLine("test");
                Tva item = new Tva
                {
                    Id = (int)reader["tv_id"],
                    Taux = (int)reader["tv_taux"]
                };
                items.Add(item);
            }
            connection.Close();

            return items;
        }
    }
}
