﻿using Generateur_Factures.Classes;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generateur_Factures.Dao
{
    public class VehiculeDao
    {
        public static List<Vehicule> GetAll()
        {

            List<Vehicule> items = new List<Vehicule>();
            MySqlConnection connection = Dao.Connection;
            try
            {
                connection.Close();
            }
            catch
            {

            }
            connection.Open();
            MySqlCommand cmd = connection.CreateCommand();
            cmd.CommandText = "SELECT * FROM `vehicule`";
            cmd.Prepare();
            cmd.ExecuteNonQuery();
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Console.WriteLine("test");
                Vehicule item = new Vehicule
                {
                    Id = (string)reader["ve_id"],
                    NumSerie = (string)reader["ve_num_serie"],
                    Gamme = (string)reader["ve_gamme"],
                    Immat = (string)reader["ve_immat"],
                    Mec = (DateTime)reader["ve_mec"],
                    Livraison = (DateTime)reader["ve_livraison"],
                    Modele = (string)reader["ve_modele"],
                    Tvv = (string)reader["ve_tvv"]
                };
                items.Add(item);
            }
            connection.Close();

            return items;
        }
    }
}
