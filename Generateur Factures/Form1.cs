﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iText;
using MySql;
using Generateur_Factures.Classes;
namespace Generateur_Factures
{
    public partial class Form1 : Form
    {
        public  List<Facture> factures;
        public  List<Client> clients;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
         //   lstFactures = factures;
            Facture facture = new Facture();
            factures = Facture.GetAll();
            lstFactures.DisplayMember = "Id";
            foreach(Facture facture1 in factures)
            {
                lstFactures.Items.Add(facture1);
            }
            
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            editForm editForm = new editForm((Facture)lstFactures.SelectedItem);
            editForm.Show();
        }
   }
}
