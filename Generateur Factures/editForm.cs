﻿using Generateur_Factures.Classes;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Generateur_Factures.Dao;
using iText;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Kernel.Font;
using iText.IO.Font;
using iText.Layout.Element;

namespace Generateur_Factures
{
    public partial class editForm : Form
    {
        Facture facture;
        public editForm(Facture factureRecue)
        {
            facture = factureRecue;
            InitializeComponent();
        }

        private void editForm_Load(object sender, EventArgs e)
        {
            textBox1.Text = Convert.ToString(facture.Id);
            dateTimePicker1.Value = facture.Date_Paie;
            dateTimePicker2.Value = facture.Date_Fact;
            textBox2.Text = facture.Accueil;
            MySqlConnection connection = Dao.Dao.Connection;
            try
            {
                connection.Close();
            }
            catch
            {

            }
            connection.Open();
            MySqlCommand cmd = connection.CreateCommand();
            cmd.CommandText = "SELECT * FROM `infovoiture` where facture = " + facture.Id ;
            cmd.Prepare();
            cmd.ExecuteNonQuery();
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                textBox3.Text = (string)reader["NumeroSerie"];
                textBox4.Text = (string)reader["Immatriculation"];
            }
            reader.Close();

            cmd.CommandText = "SELECT * FROM `infoclient` where facture = " + facture.Id;
            cmd.Prepare();
            cmd.ExecuteNonQuery();
            reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                textBox5.Text = (string)reader["NomPrenom"];
                textBox6.Text = (string)reader["NumeroCompte"];
            }

            reader.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "pdf files (*.pdf)|*.pdf";
            saveFileDialog1.ShowDialog();
            var chemin = saveFileDialog1.FileName;
            MessageBox.Show(chemin);
            var writer = new PdfWriter(chemin);
            var pdf = new PdfDocument(writer);
            var document = new Document(pdf);
            // Create a PdfFont
            var font = PdfFontFactory.CreateFont(FontConstants.TIMES_ROMAN);
            // Add a Paragraph
            // Create a List
            document.Add(new Paragraph("Details facture N° " + facture.Id).SetFont(font));
            // Create a List 
            List list = new List()
                .SetSymbolIndent(12)
                .SetListSymbol("->")
                .SetFont(font);
            // Add ListItem objects 
            list.Add(new ListItem("Date de facturation : " + facture.Date_Fact))
                .Add(new ListItem("Date de paiement : " + facture.Date_Paie))
                .Add(new ListItem("Agent d'accueil : " + facture.Accueil));
            // Add the list 
            document.Add(list);

            document.Add(new Paragraph("Details client").SetFont(font));
            // Create a List 
            list = new List()
                .SetSymbolIndent(12)
                .SetListSymbol("->")
                .SetFont(font);
            // Add ListItem objects 
            list.Add(new ListItem("Nom Prénom : " + textBox5.Text))
                .Add(new ListItem("Numéro de compte : " + textBox6.Text));

            // Add the list 
            document.Add(list);

            document.Add(new Paragraph("Details véhicule").SetFont(font));
            // Create a List 
            list = new List()
                .SetSymbolIndent(12)
                .SetListSymbol("->")
                .SetFont(font);
            // Add ListItem objects 
            list.Add(new ListItem("Immatriculation : " + textBox4.Text))
                .Add(new ListItem("Numéro de série : " + textBox3.Text));

            // Add the list 
            document.Add(list);



            var table = new Table(new float[] { 4, 1, 3, 4, 3, 3, 3, 3, 1 });
            document.Add(table);


            document.Close();

        }
    }
}
