-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: renault
-- ------------------------------------------------------
-- Server version	5.5.5-10.4.10-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `element`
--

DROP TABLE IF EXISTS `element`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `element` (
  `el_reference` varchar(45) NOT NULL,
  `el_description` varchar(5000) DEFAULT NULL,
  `el_prix_ht_actuel` float DEFAULT NULL,
  `el_type` enum('MO','Piece') DEFAULT NULL,
  PRIMARY KEY (`el_reference`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `element`
--

LOCK TABLES `element` WRITE;
/*!40000 ALTER TABLE `element` DISABLE KEYS */;
INSERT INTO `element` VALUES ('0048','OG REVISION VIDANGE MOTEUR NIVEAUX PRESSION REMPLACEMENT FILTRE HUILE CONTROLE FREINS AV ECLAIRAGE PNEUMATIQUES PASSAGE CLIP TEST BATTERIE MIDTRONICS',45,'MO'),('152095084R','CARTOUCHE FILTRE HUILE',14.33,'Piece'),('272773277R','FILTRE HABITACLE (RP : 272778214R)',22.92,'Piece'),('3123','PERMUTATION DES 4 PNEUS ETE/HIVER',45,'MO'),('6170','OG REMPL FILTRE HABITACLE',45,'MO'),('7711238969','LAVE-GLACE HIVER',2.58,'Piece'),('7711771317','MICHELIN 195/55 R16 91H E B 1 68 ALPIN 5 XL',122,'Piece'),('8200641648','RONDELLE ETANCHEI',2.1,'Piece'),('DECH','PARTICPATION RECYCLAGE DES DECHETS',2,'Piece'),('VX500R','YACCO VX500R 10W40',4.81,'Piece');
/*!40000 ALTER TABLE `element` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-24 11:03:46
