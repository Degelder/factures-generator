-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: renault
-- ------------------------------------------------------
-- Server version	5.5.5-10.4.10-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `element_facture`
--

DROP TABLE IF EXISTS `element_facture`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `element_facture` (
  `quantité` float DEFAULT NULL,
  `prix_ht_facture` float DEFAULT NULL,
  `remise` float DEFAULT NULL,
  `fk_facture` int(11) NOT NULL,
  `fk_element` varchar(45) NOT NULL,
  PRIMARY KEY (`fk_element`,`fk_facture`),
  KEY `facture_idx` (`fk_facture`),
  KEY `element_idx` (`fk_element`),
  CONSTRAINT `element` FOREIGN KEY (`fk_element`) REFERENCES `element` (`el_reference`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `facture` FOREIGN KEY (`fk_facture`) REFERENCES `facture` (`fa_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `element_facture`
--

LOCK TABLES `element_facture` WRITE;
/*!40000 ALTER TABLE `element_facture` DISABLE KEYS */;
INSERT INTO `element_facture` VALUES (1.3,45,NULL,354780,'0048'),(1,14.33,NULL,354780,'152095084R'),(1,22.92,NULL,354780,'272773277R'),(1,45,NULL,354780,'3123'),(0.2,45,NULL,354780,'6170'),(1,2.58,NULL,354780,'7711238969'),(1,122,25,354780,'7711771317'),(1,2.1,NULL,354780,'8200641648'),(1,2,NULL,354780,'DECH'),(1,4.81,NULL,354780,'VX500R');
/*!40000 ALTER TABLE `element_facture` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-24 11:03:46
